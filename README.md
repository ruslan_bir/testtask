# Testtask

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.7.

## Estimate

Task 1: 30 min (after finish: ~20-25min)
Task 2: 10 min (after finish: ~10min)
Task 3: 30 min (after finish: ~10min)
Task 4: 10 min (after finish: ~10min)
Task 5: 30 min (after finish: ~30min)

## Install

You need to have [Node.js](https://nodejs.org/en/download/current/) on your computer.
You need to install "Angular-cli": `npm install -g @angular/cli@1.4.7`.
Run `npm install` to install the packages for Node.js needed this project.

## Static web-server (local server)

First you need to create app build (--prod (--aot)).
Run `node server.js` for a local server. Navigate to `http://localhost:8080/`.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Syntax check

Run `ng lint` to execute syntax check via tslint.
