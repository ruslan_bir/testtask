import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UsersService {
  BASE_URL = 'http://jsonplaceholder.typicode.com';
  constructor(private HTTP: Http) { }
  getUsers() {
    return this.HTTP.get(`${this.BASE_URL}/users`, {}).toPromise()
      .then(this.extractData)
      .catch(this.handleErr);
  }
  private extractData(res: Response) {
    const body = res.json();
    return body;
  }
  private handleErr(error: Response | any) {
    const err = error;
    return err;
  }
}
