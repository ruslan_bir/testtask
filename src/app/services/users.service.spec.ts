import { TestBed, inject } from '@angular/core/testing';

import { UsersService } from './users.service';
import { HttpModule } from '@angular/http';

describe('UsersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [UsersService]
    });
  });

  it('should be created', inject([UsersService], (service: UsersService) => {
    expect(service).toBeTruthy();
  }));
});
