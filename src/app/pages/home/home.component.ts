import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  users: Array<object>;
  user: object;
  userId: number;
  showDetails = false;
  router: Router;
  constructor(private restUsers: UsersService, _router: Router, private route: ActivatedRoute) {
    this.router = _router;
  }
  getUsers() {
    this.restUsers.getUsers().then(
      res => {
        this.users = res;
        if (this.userId !== undefined) { this.details(this.userId); }
      },
      err => console.log(err)
    );
  }
  details(i: number) {
    console.log(this.users);
    this.user = this.users[i];
    this.showDetails = true;
  }
  goto(page: string, id: number) {
    let param: object = {};
    if (id >= 0) { param = { u: id}; }
    this.router.navigate(['/' + page, param]);
  }
  ngOnInit() {
    const sub = this.route.params.subscribe(
      data => {
        if (data.u) {
          this.userId = Number(data.u);
        } else {
          this.showDetails = false;
          this.userId = undefined;
          console.log('modal is closed');
        }
        this.getUsers();
      },
      err => console.log(err)
    );
  }
}
